import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, TextInput, ScrollView, ActivityIndicator } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import PushNotification from "react-native-push-notification"
import BackgroundFetch from "react-native-background-fetch";
import { connect } from 'react-redux'
import ValidationRules from '../forms/valudationRules'
import Modal from "react-native-modal"
import { api } from '../Api/api'

class Profile extends React.Component {

  static navigationOptions = ({navigation}) => ({
    //header: null
    headerRight: () => <TouchableOpacity 
          onPress={()=> navigation.getParam('reload')()}
          style={{flex: 1,alignItems: 'center',justifyContent: 'center'}}
          >
            <Image style={{width: 25, height: 25, marginRight : 20}} source={require('../Image/sign.png')} />
        </TouchableOpacity>
  });


  constructor(props) {
    super(props);
    this.state = {
      api: api(),
      iduser : this.props.login.id,
      email : this.props.login.email,
      nom : this.props.login.nom,
      prenom : this.props.login.prenom,
      data : null,
      status: null,
      laoding : false,
      currentpassword:false,
      password:false,
      repeatpassword:false,
      isModalVisible: false,
      hasErrors: false,
      form:{
        currentpassword:{
          value:"",
          valid:false,
          type:"textinput",
          rules:{
              isRequired:true
          }
        },
        password:{
            value:"",
            valid:false,
            type:"textinput",
            rules:{
                isRequired:true,
                minLength:6
            }
        },
        repeatpassword:{
          value:"",
          valid:false,
          type:"textinput",
          rules:{
              isRequired:true,
              minLength:6
          }
        }
    }
    }
    
  }
  
  reload = () => {
    const action = { type: "LOGIN_STATUS", value: {id : '' , email : '', nom : '', prenom : ''}}
    this.props.dispatch(action)
    AsyncStorage.removeItem('status');
    AsyncStorage.removeItem('date');
    BackgroundFetch.stop("com.foo.customtask")
    PushNotification.cancelAllLocalNotifications()
    this.props.navigation.navigate('AuthLoading')
  }

   componentDidMount() {

    const {navigation} = this.props
    
    navigation.setParams({
      reload: this.reload,
    })

     navigation.addListener('didFocus',() =>{
      this.initial()
    })
  }
  
  initial =() =>{
    const formCopy = this.state.form;
    for(let key in formCopy){
      formCopy[key].valid = false
      formCopy[key].value = ""
    }
    this.setState({
      form:formCopy,
      currentpassword:false,
      password:false,
      repeatpassword:false,
      hasErrors: false
    })
  }

  updateInput = (name,value) =>{

      let formCopy = this.state.form;
      formCopy[name].value = value;

      let rules = formCopy[name].rules
      let valid = ValidationRules(value, rules);

      formCopy[name].valid = valid;

      if(valid){
        if(name == "repeatpassword" && this.state.form.password.value != this.state.form.repeatpassword.value){
          this.setState({
            [name]: true
          })
        }else{
          this.setState({
            [name]: false
          })
        }

      }else{
        this.setState({
          [name]: true
        })
      }

      this.setState({
          form:formCopy, hasErrors:false
      })

  }

  update =() => {

    let isFormValid = true;
    const formCopy = this.state.form;

    for(let key in formCopy){
        isFormValid = isFormValid && formCopy[key].valid;
        if(!formCopy[key].valid){
          this.setState({
            [key]: true
          })
        }
    }

    if(formCopy.password.value != formCopy.repeatpassword.value || formCopy.repeatpassword.value == ""){
      isFormValid = false
      this.setState({
        repeatpassword: true
      })
    }else{
      this.setState({
        repeatpassword: false
      })
    }

    if(formCopy.currentpassword.value == formCopy.password.value){
      isFormValid = false
      this.setState({
        password: true
      })
    }else{
      this.setState({
        password: false
      })
    }

    if(isFormValid){
      this.setState({ isModalVisible: true});
    }
    
  }

  updatedata =() => {

    this.setState({
      laoding : true
    })

    let data = {
      "current_password" : this.state.form.currentpassword.value,
      "password_personne" : this.state.form.password.value
    }

    function timeoutPromise(timeout, err, promise) {
      return new Promise(function(resolve,reject) {
        promise.then(resolve,reject);
        setTimeout(reject.bind(null,err), timeout);
      });
    }

    timeoutPromise(5000, new Error('Timed Out!'), fetch(this.state.api+'/personne/password/'+this.state.iduser, {
      method: 'PUT',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
      }))
      .then((response) => response.json())
      .then((responseJson) => {

        if (responseJson === 'password updated') {
          
          this.reload()

        }else if(responseJson === 'wrong current password'){
          
          this.setState({ 
            isModalVisible: false,
            currentpassword: true,
            laoding : false
          });
        }
        
      })
      .catch((error) => {
        this.setState({ 
          isModalVisible: false, 
          hasErrors:true,
          laoding : false
        });
      });

  }

  formHasErrors = () => (
    this.state.hasErrors ?
            <Text style={{fontSize:12, margin: 5,color : 'red'}}>Connection error !</Text>
    :
    null 
  )
  
  cancelmodal = () => {
    this.setState({isModalVisible: false});
    this.initial()
  }

  renderModalContent = () => (
    <View style={styles.content}>
      {this.loadingscreen()}
      <View style={styles.sryletochable}>
        <Text style={styles.contentTitle}>Save update!</Text>
      </View>
      <Text style={{fontSize:13, margin: 5,color : 'orange'}}>
        Tips: App will log out !
      </Text>  
      <View style={{ flexDirection: 'row', marginTop: 10 }}>
        <TouchableOpacity
          style={styles.btnmodal}
          onPress={this.updatedata}>
          <Text style={styles.textsaveupdate} numberOfLines={2}>
           save
                    </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btnmodal}
          onPress={this.cancelmodal}>
          <Text style={styles.textsaveupdate} numberOfLines={2}>
            cancel
                    </Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  loadingscreen =() => {
    return(
      <View style={styles.containerlaoding} >
        <ActivityIndicator animating={this.state.laoding} size="large" color="#255282" />
      </View>
    )
  }


render() {

  const {currentpassword,password,repeatpassword} = this.state
    
  return (
    <View style={styles.container}>
      <ScrollView style={styles.list}>
      <View style={styles.flatcontent}>
        <View style={styles.sryletochable}>
                <Image
                      style={styles.imageinput}
                      source={require('../Image/user.png')}
                    />
          <Text>{this.state.nom} {this.state.prenom}</Text>
        </View>
        <View style={styles.sryletochable}>
                  <Image
                      style={styles.imageinput}
                      source={require('../Image/mail.png')}
                    />
          <Text>{this.state.email}</Text>
        </View>
      </View>
      <View style={styles.containerlist}>
        <Text style={{fontSize: 14}}>Change Password</Text>
      </View>
        <View style={styles.flatcontent}>
          <View style={[styles.container_input,{borderColor: currentpassword ? 'red' : '#d6d7da' }]} >
          <TextInput 
                style={styles.input}
                placeholder="Enter your current password"
                type={this.state.form.currentpassword.type}
                value={this.state.form.currentpassword.value}
                onChangeText={ value => this.updateInput("currentpassword",value)}
                secureTextEntry
                returnKeyType="next"
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                blurOnSubmit={false}
          />
          </View>
          <View style={[styles.container_input,{borderColor: password ? 'red' : '#d6d7da'}]} >
          <TextInput 
                style={styles.input}
                placeholder="Enter your new password"
                type={this.state.form.password.type}
                value={this.state.form.password.value}
                onChangeText={ value => this.updateInput("password",value)}
                secureTextEntry
                ref={(input) => { this.secondTextInput = input; }}
                returnKeyType="next"
                onSubmitEditing={() => { this.thirdTextInput.focus(); }}
                blurOnSubmit={false}
          />
          </View>
          <View style={[styles.container_input,{borderColor: repeatpassword ? 'red' : '#d6d7da'}]} >
          <TextInput 
                style={styles.input}
                placeholder="Repeat password"
                type={this.state.form.repeatpassword.type}
                value={this.state.form.repeatpassword.value}
                onChangeText={ value => this.updateInput("repeatpassword",value)}
                secureTextEntry
                ref={(input) => { this.thirdTextInput = input; }}
          />
          </View>
          <Text style={{fontSize:12, margin: 5,color : 'orange'}}>Tips: your password should be more than 6 character !</Text>
          {this.formHasErrors()}
          <View style={styles.buttonStyleAndroid}>
                      <TouchableOpacity 
                          style={styles.gourl}
                          onPress={this.update}>
                        <Text style={styles.textgourl}>
                          Update
                        </Text>
                      </TouchableOpacity>
          </View>
          
          <Modal isVisible={this.state.isModalVisible}
              animationInTiming={500}
              animationOutTiming={500}
              backdropTransitionInTiming={300}
              backdropTransitionOutTiming={300}>
              {this.renderModalContent()}
            </Modal>

        </View>
      </ScrollView>
   </View>
  )
}}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#E3E9ED'
  },
  sryletochable: {
    flexDirection: 'row',
    borderRadius: 4,
    borderBottomWidth: 0.7,
    borderColor: '#d6d7da',
    margin: 4,
    padding: 8
  },
  containerlist: {
    backgroundColor : '#bdbcbc',
    margin: 12,
    marginTop: 5,
    padding: 8,
    borderRadius: 4,
  },
  gourl :{
    alignItems: 'center',
    padding:8,
    paddingLeft:35,
    paddingRight:35,
    borderRadius: 10,
    color: 'white',
    backgroundColor : '#255282'
  },
  list: {
    flex: 1,
    backgroundColor: '#e6e6e6',
    paddingTop : 8
  },
  flatcontent: {
    backgroundColor : 'white',
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    margin: 12,
    marginTop: 5,
    padding: 10
  },
  container_input: {
    flexDirection: 'row',
    backgroundColor : 'white',
    borderRadius: 4,
    borderWidth: 0.8,
    margin: 5,
    marginTop: 10,
  },
  input: {
    fontSize:14,
    padding: 5,
    width: 300,
    height: 44,
  },
  buttonStyleAndroid:{
    flexDirection : 'row',
    justifyContent: 'center',
    marginTop:20,
    marginBottom: 25
  },
  gourl :{
    flexDirection: "row",
    alignItems: 'center',
    padding:8,
    paddingLeft:30,
    paddingRight:30,
    borderRadius: 10,
    color: 'white',
    backgroundColor : '#255282'
  },
  textgourl:{
    color: 'white',
    fontSize: 15
  },
  textsaveupdate:{
    color: 'white',
    fontSize: 13
  },
  imageinput: {
    width: 18,
    height: 18,
    marginRight: 6
  },
  content: {
    backgroundColor: 'white',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  contentTitle: {
    fontSize: 16
  },
  btnmodal: {
    alignItems: 'center',
    padding: 8,
    margin: 10,
    paddingLeft: 30,
    paddingRight: 30,
    borderRadius: 10,
    color: 'white',
    backgroundColor: '#255282'
  },
  containerlaoding: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

const mapStateToProps = state => {
  return{
    login : state.setLogin.login
  } 
}

export default connect(mapStateToProps)(Profile);
