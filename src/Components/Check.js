// Home.js

import React from 'react'
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux'

class Check extends React.Component {
  
  static navigationOptions = {
    headerShown: false
  };

 	constructor(props) {
	  super(props);
  }
  
  componentDidMount(){

    this.props.navigation.navigate(this.props.login.id ? 'SignedIn' : 'SignedOut');
 
  }

	render() {
    return (
       <View style={styles.container}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    )
  }
  }

  const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  }
})

const mapStateToProps = state => {
  return{
    login : state.setLogin.login
  } 
}

export default connect(mapStateToProps)(Check);