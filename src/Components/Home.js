// Home.js

import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity, AppState } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux'
import QRCode from 'react-native-qrcode-generator';
import BackgroundFetch from "react-native-background-fetch";
import PushNotification from "react-native-push-notification"
import moment from 'moment';
import { api } from '../Api/api'

class Home extends React.Component {
  
  static navigationOptions = ({navigation}) => ({
    //header: null
    headerRight: () => <TouchableOpacity 
          onPress={()=> navigation.getParam('reload')()}
          style={{flex: 1,alignItems: 'center',justifyContent: 'center'}}
          >
            <Image style={{width: 25, height: 25, marginRight : 20}} source={require('../Image/sign.png')} />
        </TouchableOpacity>
  });

 	constructor(props) {
    super(props);
    this.state = {
      api: api(),
      iduser: this.props.login.id,
      i : null,
      status : null,
      date : null
    }

    PushNotification.configure({
      onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);
      },
      popInitialNotification: true,
      requestPermissions: Platform.OS === 'ios'
    });
  }

  reload = () => {
    const action = { type: "LOGIN_STATUS", value: {id : '' , email : '', nom : '', prenom : ''}}
    this.props.dispatch(action)
    AsyncStorage.removeItem('status');
    AsyncStorage.removeItem('date');
    BackgroundFetch.stop("com.foo.customtask")
    PushNotification.cancelAllLocalNotifications()
    this.props.navigation.navigate('AuthLoading')
  }
  
  componentDidMount() {

    this.props.navigation.setParams({
      reload: this.reload,
    })

    AppState.addEventListener('change', this._handleAppStateChange);
  
    this.props.navigation.addListener ('didFocus', async() =>{
        console.log("focus")
        await AsyncStorage.multiGet(["status", "date"]).then(response => {
          this.setState({
            status: JSON.parse(response[0][1]) ,
            date : JSON.parse(response[1][1])
          })
        })
        this.scheduleTask()
    })

  }

  _handleAppStateChange = (nextAppState) => {
    if (nextAppState === 'active') {
      console.log("back")
      this.scheduleTask()
    }
  }

  scheduleTask (){
    this.setState({i : 0})
    console.log("status : "+ this.state.status)
    console.log("date : "+ this.state.date)
    //BackgroundFetch.finish("com.foo.customtask");
  // Step 1:  Configure BackgroundFetch as usual.
      BackgroundFetch.configure({
        minimumFetchInterval: 15,
        enableHeadless : true,
        stopOnTerminate: false,
        startOnBoot: true,
        requiredNetworkType: BackgroundFetch.NETWORK_TYPE_ANY
      }, async (taskId) => {

        console.log("[BackgroundFetch] taskId: ", taskId);

        // Use a switch statement to route task-handling.
        switch (taskId) {
          case 'com.foo.customtask':

          //fetch
          let response = await fetch(this.state.api+'/personne/getstatus/'+this.state.iduser);
          let responseJson = await response.json();
          console.log(responseJson);
          
          if (responseJson.length === 1) {
            const status = responseJson[0].status_personne

            if (status != this.state.status) {
              console.log('def status!')
              AsyncStorage.setItem('status', JSON.stringify(status))
              this.setState({status : status})
            }

            if (status) {
                console.log('first!')

                let response = await fetch(this.state.api+'/personne/dateControle/'+this.state.iduser);
                let responseJsondate = await response.json();
                console.log(responseJsondate);

                if (responseJsondate.length === 1) {
                const datenot = responseJsondate[0].prochain_controle_personne

                  if (datenot != this.state.date && datenot != null) {

                    AsyncStorage.setItem('date', JSON.stringify(new Date(datenot)))
                    this.setState({date : datenot, i : 5})

                    PushNotification.localNotification({
                      id: "1",
                      title: "Precautions Covid 19", 
                      message: "Hi there, your control is done, thanks for coming.", 
                      ticker: "Hi there !",
                      autoCancel: true,
                      allowWhileIdle: true,
                    })
    
                    PushNotification.localNotificationSchedule({
                      id: "2",
                      title: "Precautions Covid 19", 
                      message: "Hi there, it's time to take your next control.", 
                      ticker: "Hi there !",
                      autoCancel: false,
                      allowWhileIdle: true,
                      date: new Date(datenot)
                    });

                  }else{
                    this.setState({i : this.state.i + 1})
                  }
                }

            }else{
              this.setState({i : this.state.i + 1})
            }
                          
          }
            //end fetch

            console.log("i :" +this.state.i)
            console.log("Received custom task");
            break;
          default:
            console.log("Default fetch task");
        }
        
        // Finish, providing received taskId.
        BackgroundFetch.finish(taskId);
        if (this.state.i == 5) {
          console.log("stop at :" +this.state.i)
          BackgroundFetch.stop(taskId)
        }
        console.log("end");
      });
      
      BackgroundFetch.scheduleTask({
        taskId: "com.foo.customtask",
        forceAlarmManager: true,
        delay: 120000, // <-- milliseconds
        periodic: true
      });
  }


	render() {
    return (
      <View style={styles.container}>
          <View style={{flex: 1,alignItems: 'center'}}>
            <View style={{width: 120 ,margin: 60,borderBottomWidth:3,borderBottomColor:'#747577'}}></View>
          </View>
            <QRCode
            value={this.state.iduser}
            size={280}
            bgColor='#255282'
            fgColor='white'/>
          
          <View style={{flex: 1,alignItems: 'center'}}>
            <View style={{width: 120 ,margin: 60,borderBottomWidth:3,borderBottomColor:'#747577'}}></View>
          </View>
      </View>
    )
  }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#E3E9ED'
    },
})


const mapStateToProps = state => {
  return{
    date : state.setDate.date,
    login : state.setLogin.login
  } 
}

export default connect(mapStateToProps)(Home)

//