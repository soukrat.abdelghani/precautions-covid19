import React from 'react'
import { StyleSheet, View, TextInput, TouchableOpacity, Text, Image ,ImageBackground, ActivityIndicator, Animated, Easing } from 'react-native'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage';
import ValidationRules from '../../forms/valudationRules'
import { api } from '../../Api/api'

class Login extends React.Component {

  static navigationOptions = {
    headerShown: false
  };

  constructor(props) {
    super(props)
    this.state = {
      api: api(),
      errorinput: "Email or Password incorrect !",
      hasErrors:false,
      laoding : false,
      form:{
        email:{
            value:"",
            valid:false,
            type:"textinput",
            rules:{
                isRequired:true,
                isEmail:true
            }
        },
        password:{
            value:"",
            valid:false,
            type:"textinput",
            rules:{
                isRequired:true,
                minLength:4
            }
        }
    },
    sellAnime: new Animated.Value(0),
    itAnim: new Animated.Value(0),
    backImage: new Animated.Value(0),
    login: new Animated.Value(0)
  }
}

  login = () =>{

    let data = {
      "mail_personne" : this.state.form.email.value,
      "password_personne" : this.state.form.password.value
    }

    function timeoutPromise(timeout, err, promise) {
      return new Promise(function(resolve,reject) {
        promise.then(resolve,reject);
        setTimeout(reject.bind(null,err), timeout);
      });
    }

    timeoutPromise(5000, new Error('Timed Out!'), fetch(this.state.api+'/personne/login', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
        }))
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.length === 1 && responseJson != "undefined") {

          const action = { type: "LOGIN_STATUS", value: 
                          {id : responseJson[0].id_personne.toString() , 
                          email : responseJson[0].mail_personne,
                          nom : responseJson[0].nom_personne,
                          prenom : responseJson[0].prenom_personne} }
          this.props.dispatch(action)
          
          AsyncStorage.multiSet([['status', JSON.stringify(responseJson[0].status_personne)], 
                                  ['date', JSON.stringify(responseJson[0].prochain_controle_personne)]]);

          this.setState({
            laoding : false
          })
          this.props.navigation.navigate('SignedIn')
          
          }else{
            this.setState({
              hasErrors:true,
              laoding : false,
              errorinput: "Email or Password incorrect !"
            })
          }

        })
        .catch((error) => {
          this.setState({
            hasErrors:true,
            laoding : false,
            errorinput: "Connection error !",
          })
        });
  }

  updateInput = (name,value) =>{
    this.setState({
        hasErrors:false
    })

    let formCopy = this.state.form;
    formCopy[name].value = value;

    let rules = formCopy[name].rules
    let valid = ValidationRules(value, rules);

    formCopy[name].valid = valid;

    this.setState({
        form:formCopy
    })
  }

  formHasErrors = () => (
    this.state.hasErrors ?
            <Text style={styles.errorLabel}>{this.state.errorinput}</Text>
    :
    <Text></Text>  
  )

  
  loginUser =() => {

    this.setState({
      laoding : true
    })

    let isFormValid = true;
    let formToSubmit = {};
    const formCopy = this.state.form;

    for(let key in formCopy){
        isFormValid = isFormValid && formCopy[key].valid;
        formToSubmit[key] = formCopy[key].value;
    }

    if(isFormValid){
      this.login();
    }else{
        this.setState({
            hasErrors:true,
            laoding : false,
            errorinput: "Email or Password incorrect !"
        })
    }
  }

  loadingscreen =() => {
    return(
      <View style={styles.container} >
        <ActivityIndicator animating={this.state.laoding} size="large" color="#255282" />
      </View>
    )
  }

  componentDidMount(){
    Animated.sequence([
        Animated.timing(this.state.sellAnime,{
            toValue:1,
            duration:1000,
            easing:Easing.easeOutCubic,
            useNativeDriver: true
        }),
        Animated.timing(this.state.itAnim,{
            toValue:1,
            duration:500,
            easing:Easing.easeOutCubic,
            useNativeDriver: true
        })
    ]).start(()=>{
        
    })
}

  render() {
  return (
    <View style={styles.formInputContainer} >
      <ImageBackground  source={require('../../Image/Background.png')} style={styles.imageBackground} >
      
      <Animated.View style={{
                        opacity: this.state.sellAnime, 
                        transform: [{translateY: this.state.sellAnime.interpolate({
                          inputRange:[0,1],
                          outputRange:[100,0]
                        })}]
                        }}>
      <Text style={{fontSize: 22}}>Precautions Covid 19</Text>

      <View style={styles.action_container}>
                  <Image
                      style={styles.imagemaps}
                      source={require('../../Image/logo.png')}
                    />
      </View>
      </Animated.View>
      {this.loadingscreen()}
      <Animated.View style={{
                        opacity: this.state.itAnim,
                        alignItems: 'center',
                        justifyContent: "center"
                        }}>
      <Text style={{fontSize: 20,color: '#255282'}}>Sign in!</Text>

      <View style={styles.container_input} >
                 <Image
                      style={styles.imageinput}
                      source={require('../../Image/user.png')}
                    />
      <TextInput 
          style={styles.input}
          placeholder="Enter your email"
          autoCapitalize={"none"}
          type={this.state.form.email.type}
          value={this.state.form.email.value}
          onChangeText={ value => this.updateInput("email",value)}
          keyboardType={"email-address"}
          returnKeyType="next"
          onSubmitEditing={() => { this.secondTextInput.focus(); }}
          blurOnSubmit={false}
      />
      </View>

      <View style={styles.container_input} >
                  <Image
                      style={styles.imageinput}
                      source={require('../../Image/password.png')}
                    />
      <TextInput 
              style={styles.input}
              placeholder="Enter your password"
              type={this.state.form.password.type}
              value={this.state.form.password.value}
              onChangeText={ value => this.updateInput("password",value)}
              secureTextEntry
              ref={(input) => { this.secondTextInput = input; }}
      />
      </View>

      <View style={styles.errorContainer}>
        {this.formHasErrors()} 
      </View>

      <View style={styles.buttonStyleAndroid}>
                      <TouchableOpacity 
                          style={styles.gourl}
                          onPress={this.loginUser}>
                        <Text style={styles.textgourl} numberOfLines={2}>
                          Login
                        </Text>
                      </TouchableOpacity>
      </View>
      </Animated.View>
      </ImageBackground> 
    </View>
  )
}}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  formInputContainer:{
    flex: 1,
    flexDirection: "column"
  },
  imageBackground: {
    flex: 1,
    resizeMode: "cover",
    alignItems: 'center',
    justifyContent: "center"
  },
  action_container: {
    marginTop: 10
  },
  buttonStyleAndroid:{
    flexDirection : 'row',
    marginTop:25,
    marginBottom: 40
  },
  container_input: {
    flexDirection: 'row',
    borderBottomWidth:2,
    borderBottomColor:'#eaeaea',
    marginTop: 15,
  },
  input: {
    fontSize:16,
    padding: 5,
    width: 300,
    height: 44,
  },
  imageinput: {
    width: 20,
    height: 20,
    marginTop:10
  },
  gourl :{
    flexDirection: "row",
    alignItems: 'center',
    padding:8,
    paddingLeft:30,
    paddingRight:30,
    borderRadius: 10,
    color: 'white',
    backgroundColor : '#255282'
  },
  textgourl:{
    color: 'white',
    fontSize: 16
  },
  imagemaps: {
    width: 180,
    height: 120,
    marginTop: 20,
    marginBottom: 20
  },
  errorContainer:{
      marginTop:15
  },
  errorLabel:{
      color: "red",
      fontSize: 12
  }
});

const mapStateToProps = state => {
  return{
    login : state.setLogin.login
  } 
}

export default connect(mapStateToProps)(Login);
