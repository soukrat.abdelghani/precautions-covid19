import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, Switch, Platform, FlatList, AppState } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import PushNotification from "react-native-push-notification"
import BackgroundFetch from "react-native-background-fetch";
import { connect } from 'react-redux'
import moment from 'moment';
import { api } from '../Api/api'

class Notification extends React.Component {

  static navigationOptions = ({navigation}) => ({
    //header: null
    headerRight: () => <TouchableOpacity 
          onPress={()=> navigation.getParam('reload')()}
          style={{flex: 1,alignItems: 'center',justifyContent: 'center'}}
          >
            <Image style={{width: 25, height: 25, marginRight : 20}} source={require('../Image/sign.png')} />
        </TouchableOpacity>
  });


  constructor(props) {
    super(props);
    this.state = {
      api: api(),
      iduser : this.props.login.id,
      data : null,
      status : null
    }

    PushNotification.configure({
      onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);
      },
      popInitialNotification: true,
      requestPermissions: Platform.OS === 'ios'
    });
    
  }
  
  reload = () => {
    const action = { type: "LOGIN_STATUS", value: {id : '' , email : '', nom : '', prenom : ''}}
    this.props.dispatch(action)
    AsyncStorage.removeItem('status');
    AsyncStorage.removeItem('date');
    BackgroundFetch.stop("com.foo.customtask")
    PushNotification.cancelAllLocalNotifications()
    this.props.navigation.navigate('AuthLoading')
  }

  toggleSwitch =()=>{
    
    let data = {
      "status_personne" : !this.state.status
    }

    fetch(this.state.api+'/personne/status/'+this.state.iduser, {
      method: 'PUT',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
      })
      .then((response) => response.json())
      .then((responseJson) => {

        if (responseJson === 'updated') {
          AsyncStorage.setItem('status', JSON.stringify(!this.state.status))
          this.setState({
            status: !this.state.status
          })

          PushNotification.cancelAllLocalNotifications()
          /*const action = { type: "SET_STATUS", value: !this.props.login.status }
          this.props.dispatch(action)*/
        }
        
      })
      .catch((error) => {
         console.log('te'+error)
      });

  }  

   componentDidMount() {

    const {navigation} = this.props
    
    navigation.setParams({
      reload: this.reload,
    })

     navigation.addListener ('didFocus', async() =>{

      await AsyncStorage.getItem('status').then(userToken => {
        this.setState({
          status: JSON.parse(userToken)
        })
      })

      fetch(this.state.api+'/controle/personneId/'+this.state.iduser)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          data : responseJson
        })
         
      })
      .catch((error) => {
         console.log("er: "+error)
      });
    })
    

    /*setInterval(() => {
      console.log("test")
     }, 4000);
     setTimeout(() => {
              console.log("timing ")
            }, 30000)*/
  }
  
  testcancel =() => {
  PushNotification.cancelAllLocalNotifications()
  }

  temperaturestatus(temperature){
    if (temperature >= 36 && temperature <= 38) {
      return(
        'green'
      )
    }else if(temperature == 39){
      return(
        'orange'
      )
    }else{
      return(
        'red'
      )
    }
  }

  showstatus (msg_controle, temperature_controle) {
    if(msg_controle === 1){
      return(
        <View>
          <View style={styles.sryletochable}>
            <Text>Temperature</Text>
            <Text style={[styles.badge_text, {backgroundColor: this.temperaturestatus(temperature_controle)}]}>{temperature_controle}°</Text>
          </View>
          <View style={styles.sryletochable}>
          <Text>Status control</Text>
          <Text style={[styles.badge_text, {backgroundColor: 'green'}]}>done</Text>
          </View>
        </View>
      )
    }else if (msg_controle === 2){
      return(
        <View>
          <View style={styles.sryletochable}>
            <Text>Temperature</Text>
            <Text style={[styles.badge_text, {backgroundColor: 'orange'}]}>uncalculated</Text>
          </View>
          <View style={styles.sryletochable}>
          <Text>Status control</Text>
          <Text style={[styles.badge_text, {backgroundColor: 'red'}]}>uncompleted</Text>
          </View>
        </View>
      )
    }else if(msg_controle === 3){
      return(
        <View>
          <View style={styles.sryletochable}>
            <Text>Temperature</Text>
            <Text style={[styles.badge_text, {backgroundColor: this.temperaturestatus(temperature_controle)}]}>{temperature_controle}°</Text>
          </View>
          <View style={styles.sryletochable}>
            <Text>Mask</Text>
            <Text style={[styles.badge_text, {backgroundColor: 'orange'}]}>not wearing</Text>
          </View>
          <View style={styles.sryletochable}>
          <Text>Status control</Text>
          <Text style={[styles.badge_text, {backgroundColor: 'red'}]}>uncompleted</Text>
          </View>
        </View>
      )
    }else if(msg_controle === 4){
      return(
        <View>
          <View style={styles.sryletochable}>
            <Text>Temperature</Text>
            <Text style={[styles.badge_text, {backgroundColor: this.temperaturestatus(temperature_controle)}]}>{temperature_controle}°</Text>
          </View>
          <View style={styles.sryletochable}>
            <Text>Gel</Text>
            <Text style={[styles.badge_text, {backgroundColor: 'orange'}]}>not using</Text>
          </View>
          <View style={styles.sryletochable}>
          <Text>Status control</Text>
          <Text style={[styles.badge_text, {backgroundColor: 'red'}]}>uncompleted</Text>
          </View>
        </View>
      )
    }else if(msg_controle === 5 && temperature_controle != null){
      return(
        <View>
          <View style={styles.sryletochable}>
            <Text>Temperature</Text>
            <Text style={[styles.badge_text, {backgroundColor: this.temperaturestatus(temperature_controle)}]}>{temperature_controle}°</Text>
          </View>
          <View style={styles.sryletochable}>
          <Text>Status control</Text>
          <Text style={[styles.badge_text, {backgroundColor: 'red'}]}>uncompleted</Text>
          </View>
        </View>
      )
    }else{
      return(
        <View>
          <View style={styles.sryletochable}>
          <Text>Status control</Text>
          <Text style={[styles.badge_text, {backgroundColor: 'red'}]}>uncompleted</Text>
          </View>
        </View>
      )
    }
  }

render() {
    
  return (
    <View style={styles.container}>
      <View style={styles.sryletochable}>
        <Text style={{fontSize: 18}}>Active/Inactive</Text>
        <Switch
        trackColor={{ false: "#767577", true: "#81b0ff" }}
        thumbColor={this.state.status ? "#f5dd4b" : "#f4f3f4"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={this.toggleSwitch}
        value={this.state.status}
        disabled={!this.state.status}
        />
  </View>    
      {/*<TouchableOpacity 
                          style={styles.gourl}
                          onPress={this.testcancel}>
                        <Text style={styles.textgourl} numberOfLines={2}>
                          test
                        </Text>
      </TouchableOpacity>*/}
      <View style={styles.containerlist}>
        <Text style={{fontSize: 15}}>List of last controls</Text>
      </View>
      {this.state.data != null ?
     <FlatList
          style={styles.list}
          data={this.state.data}
          keyExtractor={(item) => item.id_controle.toString()}
          renderItem={({item}) => (
            <View style={styles.flatcontent}>
              <View style={styles.sryletochable}>
                <Text>{moment(new Date(item.date_controle)).format("DD/MM/YYYY")}</Text>
                <Text>{moment(new Date(item.date_controle)).format("HH:mm A")}</Text>
              </View>
              {this.showstatus(item.msg_controle,item.temperature_controle)}
            </View>
          )}
        />
        :
        <View style={{alignItems: 'center'}}>
          <Text style={{color: 'orange'}}>No data !</Text>
        </View>
        }
   </View>
  )
}}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#E3E9ED'
  },
  sryletochable: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor : 'white',
    borderRadius: 4,
    borderWidth: 0.6,
    borderColor: '#d6d7da',
    margin: 5,
    marginTop: 10,
    padding: 10
  },
  containerlist: {
    backgroundColor : '#bdbcbc',
    margin: 10,
    marginTop: 6,
    padding: 8,
    borderRadius: 4,
  },
  gourl :{
    alignItems: 'center',
    padding:8,
    paddingLeft:35,
    paddingRight:35,
    borderRadius: 10,
    color: 'white',
    backgroundColor : '#255282'
  },
  textgourl:{
    color: 'white',
    fontSize: 15
  },
  list: {
    flex: 1,
    backgroundColor: '#e6e6e6',
    paddingTop : 8
  },
  flatcontent: {
    backgroundColor : 'white',
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    margin: 12,
    marginTop: 0,
    padding: 10
  },
  badge_text: {
    fontSize: 12,
    color: '#fff',
    borderRadius: 4,
    padding: 3,
    paddingRight: 10,
    paddingLeft: 10
  }
});

const mapStateToProps = state => {
  return{
    login : state.setLogin.login
  } 
}

export default connect(mapStateToProps)(Notification);
