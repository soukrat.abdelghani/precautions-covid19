// Store/configureStore.js

import { createStore } from 'redux'
import { persistCombineReducers } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage';
import setDate from './Reducers/dateReducer'
import setLogin from './Reducers/loginReducer'

const rootPersistConfig = {
    key: 'root',
    storage: AsyncStorage
  }

export default createStore(persistCombineReducers(rootPersistConfig, {setDate, setLogin}))
