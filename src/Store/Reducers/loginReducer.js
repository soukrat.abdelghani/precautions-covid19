// Store/Reducers/loginReducer.js

const initialState = { login: {id : '' , email : '', nom : '', prenom : ''} }

function setLogin(state = initialState, action) {
  let nextState
  switch (action.type) {
    case 'LOGIN_STATUS':
      nextState = {
        ...state,
        login: action.value
      }
      return nextState || state
    case 'SET_STATUS':
    nextState = {
      ...state,
      login: {...state.login, status : action.value}
    }
    return nextState || state
  default:
    return state
  }
}

export default setLogin