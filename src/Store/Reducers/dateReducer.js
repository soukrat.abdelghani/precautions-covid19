// Store/Reducers/dateReducer.js

const initialState = { date: "" }

function setDate(state = initialState, action) {
  let nextState
  switch (action.type) {
    case 'SET_DATE':
      nextState = {
        ...state,
        date: action.value
      }
      return nextState || state
  default:
    return state
  }
}

export default setDate
