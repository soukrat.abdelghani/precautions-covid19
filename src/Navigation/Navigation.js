import React from 'react'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { StyleSheet, Image } from 'react-native'
import Login from '../Components/Auth/Login'
import Home from '../Components/Home'
import Notification from '../Components/Notification'
import Profile from '../Components/Profile'
import Check from '../Components/Check'


const HomeStackNavigator = createStackNavigator({
    Home: {
        screen : Home,
        navigationOptions: {
            title : "Code QR"
        }
    }
})

const NotificationStackNavigator = createStackNavigator({
    Notification: {
        screen : Notification,
        navigationOptions: {
            title : "control"
        }
    }
})

const ProfileStackNavigator = createStackNavigator({
    Profile: {
        screen : Profile,
        navigationOptions: {
            title : "Profile"
        }
    }
})

const LoginStackNavigator = createStackNavigator({
    Login: {
        screen : Login
    }
})

const CheckStackNavigator = createStackNavigator({
    Check: {
        screen : Check
    }
})

const allTabNavigator = createBottomTabNavigator({
    Home: {
        screen : HomeStackNavigator,
        navigationOptions: {
            tabBarIcon: () => {
                return <Image 
                    source={require('../Image/qr-code.png')}
                    style={styles.icon}
                />
            }
        }
    },
    Notification: {
        screen : NotificationStackNavigator,
        navigationOptions: {
            tabBarIcon: () => {
                return <Image 
                    source={require('../Image/Notification.png')}
                    style={styles.icon}
                />
            }
        }
    },
    Profile: {
        screen : ProfileStackNavigator,
        navigationOptions: {
            tabBarIcon: () => {
                return <Image 
                    source={require('../Image/user.png')}
                    style={styles.icon}
                />
            }
        }
    }
    },
    {
        tabBarOptions: {
            showLabel: false,
            showIcon: true,
            activeBackgroundColor: '#DDDDDD'
        }
    })

const styles = StyleSheet.create({
    icon: {
        width: 30,
        height: 30
    }
})


const createRootNavigator = createSwitchNavigator(
        {
            AuthLoading: {
                screen : CheckStackNavigator
            },
            SignedIn: {
                screen: allTabNavigator
            },
            SignedOut: {
                screen : LoginStackNavigator
            }
        },
        {
            initialRouteName: "AuthLoading"
        }
    )

export default createAppContainer(createRootNavigator)